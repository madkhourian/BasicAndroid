package Madkhourian.Android.Basic.Activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import Madkhourian.Android.Basic.Fragments.MyPagerAdabpter;
import Madkhourian.Android.Basic.R;

public class MyFragmentActivity extends AppCompatActivity {
ViewPager pager;
SmartTabLayout indicator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_fragment);
        pager = findViewById(R.id.pager);
        indicator = findViewById(R.id.indicator);
        MyPagerAdabpter adapter = new MyPagerAdabpter(
                getSupportFragmentManager()
        );
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
    }
}

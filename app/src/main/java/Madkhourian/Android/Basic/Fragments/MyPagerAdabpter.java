package Madkhourian.Android.Basic.Fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyPagerAdabpter extends FragmentPagerAdapter {
    public MyPagerAdabpter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        if( i == 0 )
        return FragmentA.getInstance();
        if( i == 1 )
        return FragmentB.getInstance();
       if( i == 2 )
        return FragmentC.getInstance();
       if( i == 3 )
        return FragmentD.getInstance();
       if( i == 4 )
        return FragmentE.getInstance();
       if( i == 5 )
        return FragmentF.getInstance();
    return null;

    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0 )
            return  "FRAGA";
            if(position == 1 )
            return  "FRAGB";
            if(position == 2 )
            return  "FRAGC";
            if(position == 3 )
            return  "FRAGD";
            if(position == 4 )
            return  "FRAGE";
            if(position == 5 )
            return  "FRAGF";
            return null;

    }

    @Override
    public int getCount() {
        return 6;
    }
}

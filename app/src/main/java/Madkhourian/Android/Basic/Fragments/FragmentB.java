package Madkhourian.Android.Basic.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import Madkhourian.Android.Basic.R;

public class FragmentB extends Fragment {
    static FragmentB instance;
    public static FragmentB getInstance(){
            if (instance == null)
                instance = new FragmentB();
            return instance;
             
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_a,container,false);

        TextView txt = v.findViewById(R.id.txt);
        //Get from database
        //Web Service Call 
        return v;

    }
}
